function M = multmat(R, q, T, tol)
if nargin<4; tol = 1e-9; end %max(size(A)) * eps(norm(A))?
g = size(R, 1); M = [];
for i = 1:g, M = [M; scalar_multmat(R(i, :), q, T, tol)]; end
end