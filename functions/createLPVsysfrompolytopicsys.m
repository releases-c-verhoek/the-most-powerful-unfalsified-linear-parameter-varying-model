function lpvsys = createLPVsysfrompolytopicsys(nx, np, nu, ny, ts)
    if nargin < 5, ts = 1; end
    if nargin < 4, ny = 1; end
    if nargin < 3, nu = 1; end
    
    try pver = polydec(pvec('box', [-ones(np,1), ones(np,1)])); % requires robust control toolbox
    catch, error('Requires robust control toolbox'); end

    ps = [zeros(np,1), eye(np)];
    
    A = zeros(ny, nx+1, size(pver, 2));
    B = zeros(ny, nx+1, size(pver, 2));
    for i = 1:size(pver, 2)
        sys = make_IO_vertex_system(nx, ny, nu);
        A(:,:,i) = sys{2};
        B(:,:,i) = sys{1};
    end
    %%
    
    Atrue(:,:,:,1) = reshape(getAp(A,ps(:,1), pver), ny,ny,[],1);
    Btrue(:,:,:,1) = reshape(getAp(B,ps(:,1), pver), ny,ny,[],1);
    for i = 2:size(ps,2)
        Atrue(:,:,:,i) = reshape(getAp(A,ps(:,i), pver), ny,ny,[],1)-Atrue(:,:,:,1);
        Btrue(:,:,:,i) = reshape(getAp(B,ps(:,i), pver), ny,nu,[],1)-Btrue(:,:,:,1);
    end
    
    %% create true system
    [Asys, Bsys] = createLPVcore_io_poly(np, Atrue, Btrue);
    
    % noise free and oe-type of LPV-IO forms
    lpvsys = lpvio(Asys,Bsys,ts);
    
        function Ap = getAp(A, P, Pvert)
            N = size(Pvert,2);
            f = ones(N,1);
            Aeq = [ones(1,N); Pvert];
            beq = [1;P];
            c = linprog(f,[],[],Aeq, beq,[],[], optimoptions('linprog','Display','off'));
            Ap = zeros(size(A,[1,2]));
            for ii = 1:N
                Ap = Ap + A(:,:,ii)*c(ii);
            end
        end
    
    
        function [out, sys] = make_IO_vertex_system(nx, ny, nu)
            while true
                sys = drss(nx, ny, nu);%sys.B = [zeros(nx-nu, nu);eye(nu)]; sys.C = [eye(ny), zeros(ny, nx-ny)];
                if all(abs(eig(sys))<0.90), break; end
            end
            sys.D = zeros(ny,nu);
            systf = tf(sys); systf.Variable = 'z^-1';
            out = [systf.Numerator, systf.Denominator];
        end

end





