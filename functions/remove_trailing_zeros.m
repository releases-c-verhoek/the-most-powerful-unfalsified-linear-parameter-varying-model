function R = remove_trailing_zeros(R, q, tol)
RR = reshape(R, q, length(R) / q);
n = find(sum(abs(RR), 1) > tol, 1, 'last');
R = vec(RR(:, 1:n))';
end