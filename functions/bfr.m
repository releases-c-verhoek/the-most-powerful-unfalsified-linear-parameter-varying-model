function out = bfr(y, yhat)
N = size(y,1);
assert(N == size(yhat,1));
assert(size(y,2) == size(yhat,2));
ybar = kron(ones(N,1),mean(y));
out = 100*max(1 - (sum(vecnorm(y-yhat,2,2))/N)/(sum(vecnorm(y-ybar,2,2))/N), 0);
end