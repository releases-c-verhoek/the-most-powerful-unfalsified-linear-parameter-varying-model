function R_io = LPVcoreIO2R(A, B, np, ny)
% assumes the IO as the form A(q)y = B(q)u
assert(~all(size(A)-size(B)),'Sizes of identified OE model not equal...')
R_io = [];
for i = 1:size(A,2)
    R_oe_i = [];
    for j = 1:np+1
        if length(size(A{i}.matrices))<3
            if j == 1
                R_oe_i = [R_oe_i, -B{i}.matrices(:,:,j), eye(ny)];
            else
                R_oe_i = [R_oe_i, -B{i}.matrices(:,:,j), zeros(ny)];
            end
        else
            R_oe_i = [R_oe_i, -B{i}.matrices(:,:,j), A{i}.matrices(:,:,j)];
        end
    end
    R_io = [R_oe_i R_io];
end
end