function lpvsys = R2LPVcoreIO(R, np, nu, ny, ts)
% assumes scheduling range of [-1, 1]
if nargin < 5, ts = 1; end
if size(R,1) ~= 1, error('Not sure what to do for row dim R > 1...'), end
Rp = flipud(reshape(R,(nu+ny)*(1+np),[])'); % form [... ; -Bi0 Ai0 -Bi1 Ai1 ... -Binp Ainp ; ...]
Rp = Rp/Rp(1,(1+nu):(nu+ny)); % this makes stuff difficult for non SISO systems...
A = cell(1,size(Rp,1));
B = cell(1,size(Rp,1));
% create scheduling variables
prange = cell(1,np); for i = 1:np, prange{i} = [-1, 1]; end
p = pmatrix(permute(eye(np),[1 3 2]), 'SchedulingDimension', np, 'SchedulingDomain', 'dt', 'SchedulingRange',prange);

for i = 1:size(A,2)
    bi = -Rp(i,1:nu);
    ai =  Rp(i,(1+nu):(nu+ny));
    for j = 1:np
        bi = bi - Rp(i,(j*(nu+ny))+(1:nu))*p(j);
        ai = ai + Rp(i,(j*(nu+ny))+((1+nu):(nu+ny)))*p(j);
    end
    A = {A{:,1:(i-1)}, pshift(ai, -(i-1))};
    B = {B{:,1:(i-1)}, pshift(bi, -(i-1))};
end
if abs(norm(A{1}.matrices(:))-1) > 1e-6, warning(['given R cannot be transformed into a monic polynomial for y, force to one... norm of A{1} - 1 =', num2str(norm(A{1}.matrices(:))-1)]); end
A{1} = 1;
lpvsys = lpvio(A,B,ts);
end