function M = scalar_multmat(R, q, T, tol)
R = remove_trailing_zeros(R, q, tol);
nc = length(R); n = T - nc / q + 1;
M = zeros(n, nc + (n - 1) * q);
for i = 1:n, M(i, (1:nc) + (i - 1) * q) = R; end
end