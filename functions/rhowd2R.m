function R = rhowd2R(rhod, wd, c, oe) 

%% dimensions
m = c(1); ell = c(2); n = c(3); 
[Td, nrho] = size(rhod); [Td, q] = size(wd); 
p = q - m; mp = (nrho + 1) * q - p; 

%% lifting
for t = 1:Td, wpd(t, :) = kron([1 rhod(t, :)], wd(t, :)); end 

%% (approximate) left kernel 
r = mp * (ell + 1) + n;
if exist('oe', 'var') 
    U = hank(wpd(:, 1:mp), ell+1); Y = hank(wpd(:, mp+1:end), ell+1); 
    Yh  = glra([U; Y]', mp * (ell+1), n)'; 
    H = UYT2BT(U, Yh, mp, p);
else
    H = hank(wpd, ell + 1); 
end
R = lra(H, r);
    
%% Hankel matrix constructor
function H = hank(w, L)
if ~iscell(w), H = blkhank(w, L); return, end 
N = length(w); H = []; 
for k = 1:N, H = [H blkhank(w{k}, L)]; end

function H = blkhank(w, L)
[q, T] = size(w); if T < q, w = w'; [q, T] = size(w); end, j = T - L + 1; 
if j <= 0, H = []; else, 
  H = zeros(L * q, j);
  for i = 1:L, H(((i - 1) * q + 1):(i * q), :) = w(:, i:(i + j - 1)); end
end

%% (generalized) low-rank appoximation
function R = lra(d, r)
[u, s, v] = svd(d); R = u(:, (r + 1):end)'; 

function dh = glra(T, n, r)
c = T(:, 1:n); d = T(:, n+1:end);
[u, c1] = qr(c); % assuming C is f.c.r 
c1 = c1(1:n, :); dp = u' * d; 
[ud, sd, vd] = svd(dp(n+1:end, :));
d2h = ud(:, 1:r) * sd(1:r, 1:r) * vd(:, 1:r)';
dh = u * [dp(1:n, :); d2h];

%% conversion from (u, y) to w
function BT = UYT2BT(UT, YT, m, p)
q = m + p; [mT, N] = size(UT); T = mT / m; BT = zeros(q * T, N);
for i = 1:m, BT(i:q:end, :) = UT(i:m:end, :); end
for i = 1:p, BT(m+i:q:end, :) = YT(i:p:end, :); end
