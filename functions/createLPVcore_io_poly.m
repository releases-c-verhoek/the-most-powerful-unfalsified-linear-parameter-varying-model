function [Asys, Bsys] = createLPVcore_io_poly(np, A, B)
% np integer
% A size: ny x ny x na+1 x np+1
% B size: ny x nu x nb+1 x np+1
    prange = cell(1,np); for i = 1:np, prange{i} = [-1, 1]; end
    p = pmatrix(permute(eye(np),[1 3 2]), 'SchedulingDimension', np, 'SchedulingDomain', 'dt', 'SchedulingRange',prange);
    % create polynomials for IO form
    Asys = {};
    for i = 1:size(A,3)
        ai = A(:,:,i,1);
        for j = 1:np
            ai = ai + A(:,:,i,j+1)*p(j);
        end
        Asys = {Asys{:,1:(i-1)}, pshift(ai, -(i-1))};
    end
    Bsys = {};
    for i = 1:size(B,3)
        bi = B(:,:,i,1);
        for j = 1:np
            bi = bi + B(:,:,i,j+1)*p(j);
        end
        Bsys = {Bsys{:,1:(i-1)}, pshift(bi, -(i-1))};
    end
end