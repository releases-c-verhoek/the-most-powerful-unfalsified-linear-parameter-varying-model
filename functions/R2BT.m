function BT = R2BT(R, q, T, tol)
if nargin<4; tol = 1e-9; end %max(size(A)) * eps(norm(A))?
BT = null(multmat(R, q, T), tol);
end