%% ===================================================================== %%
%  Academic example used in the paper:
%    I. Markovsky, C. Verhoek, and R. Tóth. The most powerful unfalsified 
%    linear parameter-varying model." Submitted to L-CSS. 2024.
%
%  The code compares the LRA identification method discussed in the paper
%  with 3 LPV identification methods available in LPVcore.
%
%  Authors: C. Verhoek and I. Markovsky
%
%  Released under the BSD 3-Clause License. 
%  Copyright (c) 2023, Eindhoven, The Netherlands
%
%
%  Final version -- Results used in published paper.
%  Tested in ML2022b.
%% ===================================================================== %%
clearvars; clc; close all;
addpath('functions')
rng(1)
% make sure to add lpvcore to your path (lpvcore.net)

%% params
% SISO lpv-oe system with 2 scheduling variables
np = 2;   % scheduling dimension
nx = 3;   % state dimension
nu = 1;   % input dimension
ny = 1;   % output dimension

% simulation params
ts = 1;
Tsim = 200;
Tc = Tsim;

% perform experiments over nexp noise levels
nexp = 9;
noise_setup = 'oe'; % 'oe' or 'eiv'
noisevariances = [0, 2*logspace(-3,-1,nexp)];
res = {};

%% Start of the script
% input and output lag
nw = nu + ny;
na = nx;
nb = nx;

% complexity
c = [nu,max(na,nb),max(na,nb)*ny];

%% Create true system
lpvsys = createLPVsysfrompolytopicsys(nx, np);
R_0 = LPVcoreIO2R(lpvsys.A, lpvsys.B, np, ny); R0 = R_0 / R_0(1);

%% create template system for ARX identification
Atempl = 0.1*ones(ny, ny, na + 1, np + 1); 
Atempl(:,:,1,1) = 1; Atempl(:,:,1,2:(np+1)) = 0;
Btempl = 0.1*ones(ny, nu, nb + 1, np + 1); 
[Atmp, Btmp] = createLPVcore_io_poly(np, Atempl, Btempl); Atmp{1} = 1; % this is required for lpvidpoly
templatesys_arx = lpvidpoly(Atmp, Btmp, [],[],[],ts);

% for OE-RIV, we need all the basis functions (p(t-i)) in Atmp and Btmp
[Atmp, Btmp] = createLPVcore_io_poly(np, Atempl, Btempl); 
for i = 1:length(Atmp)
    for j = 1:length(Atmp), Atmp{i} = Atmp{i} + 0*Atmp{j}; Btmp{i} = Btmp{i} + 0*Btmp{j};
    end
end; Atmp{1} = 1; % this is required for lpvidpoly
templatesys_oe  = lpvidpoly([], Btmp, [],[],Atmp,[],ts);

%% Simulate estimation data
usim = rand(Tsim, nu); psim = rand(Tsim, np); 
ysim = lsim(lpvsys, psim, usim);
if strcmp(noise_setup, 'oe'), nsim = randn(Tsim, ny); nsim = norm(ysim)*nsim/norm(nsim);
else, nsim = randn(Tsim, nw); nsim = norm([usim ysim])*nsim/norm(nsim);
end

%% Validation criteria
% error between kernels
e1 = @(Rh) norm(R0 - Rh) / norm(R0);

% angle between subspaces for given scheduling
Pbar = makePbarn([ones(1,Tc);  randn(np, Tc)],nw); % make caligraphic P
BT_true = null(multmat(R0, nw*(np+1), Tc)*Pbar);
% compute angle
e2 = @(Rh) subspace(BT_true, null(multmat(Rh,  nw*(np+1), Tc)*Pbar));

% Best-fit-rate on validation set
uval = randn(Tsim, nu); pval = 2*rand(Tsim, np)-1; yval = lsim(lpvsys, pval, uval);
e3 = @(ytst) bfr(yval,ytst);

lpvarx_opt = lpvarxOptions('Display', 'off');
lpvoe_opts = lpvoeOptions('Display', 'off', 'Initialization', 'template');

%% iterate over noise variances
for idx = 1:size(noisevariances,2)
    if strcmp(noise_setup, 'oe')
        yn = noisevariances(idx) * nsim;
        res.snr(idx) = snr(ysim,yn);
        un = zeros(Tsim,nu);
    else % Add noise to data in EIV setup
        un = noisevariances(idx) * nsim(:,1);
        yn = noisevariances(idx) * nsim(:,2);
        res.snr(idx) = snr([usim ysim],[un yn]);
    end
    
    %% Identification step
    % create data-set in LPVcore
    dataset = lpviddata(ysim+yn, psim, usim+un, ts);
    
    % identify with lpv-arx (standard options)
    tic
    idedmodel_arx = lpvarx(dataset, templatesys_arx, lpvarx_opt);
    res.timearx(i) = toc;
    R_arx = LPVcoreIO2R(idedmodel_arx.A.Mat, idedmodel_arx.B.Mat, np, ny);
    yarx = lsim(lpvio(idedmodel_arx.A.Mat, idedmodel_arx.B.Mat, ts), pval, uval);
    Rarx = R_arx / R_arx(1);
    
    % identify with lpv-oe, initialize with arx estimate (=polypre initialization)
    lpvoe_opts = lpvoeOptions('Display', 'off', 'Initialization', 'template');
    tic
    idedmodel_oe_arx = lpvoe(dataset, lpvidpoly([], idedmodel_arx.B, [],[],idedmodel_arx.A,[],ts), lpvoe_opts);
    res.timeoearx(i) = toc;
    R_oe_arx = LPVcoreIO2R(idedmodel_oe_arx.F.Mat, idedmodel_oe_arx.B.Mat, np, ny);
    yoe_arx = lsim(lpvio(idedmodel_oe_arx.F.Mat, idedmodel_oe_arx.B.Mat, ts), pval, uval);
    Roe_arx = R_oe_arx / R_oe_arx(1);

    % identify with lpv-oe, initialize with RIV estimate
    lpvoe_opts = lpvoeOptions('Display', 'off', 'Initialization', 'riv');
    tic
    idedmodel_oe_riv = lpvoe(dataset, templatesys_oe, lpvoe_opts);
    res.timeoeriv(i) = toc;
    % Remove extended basis functions
    Foe_tmp = idedmodel_oe_riv.F.Mat;
    for i = 1:length(Foe_tmp), Foe_tmp{i} = Foe_tmp{i}.simplify(1e-100); end
    Boe_tmp = idedmodel_oe_riv.B.Mat;
    for i = 1:length(Boe_tmp), Boe_tmp{i} = Boe_tmp{i}.simplify(1e-100); end
    R_oe_riv = LPVcoreIO2R(Foe_tmp, Boe_tmp, np, ny);
    yoe_riv = lsim(lpvio(idedmodel_oe_riv.F.Mat, idedmodel_oe_riv.B.Mat, ts), pval, uval);
    Roe_riv = R_oe_riv / R_oe_riv(1);

    % identify with LRA method
    tic
    if strcmp(noise_setup, 'oe'), R_lra = rhowd2R(psim,[usim+un, ysim+yn],c,'oe');
    else, R_lra = rhowd2R(psim,[usim+un, ysim+yn],c);
    end
    res.timelra(i) = toc;
    ylra = lsim(R2LPVcoreIO(R_lra, np, nu, ny), pval, uval); 
    Rlra = R_lra / R_lra(1);

    %% Compare
    res.e_arx(idx)      = e1(Rarx);
    res.e_oe_arx(idx)   = e1(Roe_arx);
    res.e_oe_riv(idx)   = e1(Roe_riv);
    res.e_lra(idx)      = e1(Rlra);

    res.d_arx(idx)      = e2(Rarx);
    res.d_oe_arx(idx)   = e2(Roe_arx);
    res.d_oe_riv(idx)   = e2(Roe_riv);
    res.d_lra(idx)      = e2(Rlra);

    res.bfr_arx(idx)    = e3(yarx);
    res.bfr_oe_arx(idx) = e3(yoe_arx);
    res.bfr_oe_riv(idx) = e3(yoe_riv);
    res.bfr_lra(idx)    = e3(ylra);
    disp(idx)
end
%%
disp(res)

fprintf('Computation time:\n\tlra: %.3e\n\tarx: %.3e\n\toe:  %.3e\n\toe*: %.3e\n', mean(res.timelra),mean(res.timearx), mean(res.timeoearx), mean(res.timeoeriv))
