# Software associated with the paper "The most powerful unfalsified linear parameter-varying model"
Code and data, released for the 2024 L-CSS/CDC paper "The most powerful unfalsified linear parameter-varying model"

## Dependencies
In order to run the included code, one will need:

- The [LPVcore](https://www.lpvcore.net) toolbox for MATLAB
- The Robust Control toolbox by MATLAB

The code is tested for MATLAB 2022b.

## Additional information
If you want to quickly link to this repository you can use: [http://tinyurl.com/mpumoflpv](http://tinyurl.com/mpumoflpv).
